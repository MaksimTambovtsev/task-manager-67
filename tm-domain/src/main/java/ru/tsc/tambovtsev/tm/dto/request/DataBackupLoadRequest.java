package ru.tsc.tambovtsev.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest(@Nullable String token) {
        super(token);
    }

}
