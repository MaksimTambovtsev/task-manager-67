package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IUserGraphRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IUserService;
import ru.tsc.tambovtsev.tm.api.service.property.ISessionPropertyService;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.util.Optional;

@Service
public class UserGraphService extends AbstractService<User, IUserGraphRepository> implements IUserService {

    @Autowired
    private ISessionPropertyService propertyService;

    @Nullable
    @Autowired
    private IUserGraphRepository repository;

    @NotNull
    @Override
    public IUserGraphRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = repository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @Nullable final User user = repository.findByEmail(email);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        repository.delete(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        return repository.findById(userId).get();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User setPassword(
            @Nullable final String userId,
            @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @Nullable final Optional<User> user = repository.findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(propertyService, password);
        user.get().setPasswordHash(hash);
        repository.save(user.get());
        return user.get();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findByLogin(login);
        user.setLocked(true);
        repository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findByLogin(login);
        user.setLocked(false);
        repository.save(user);
    }

}
