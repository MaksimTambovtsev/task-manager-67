package ru.tsc.tambovtsev.tm.listener.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.tambovtsev.tm.listener.AbstractListener;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.util.DateUtil;

import java.util.List;

@Getter
@Setter
@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    public String getArgument() {
        return null;
    }

    public void renderTasks(@Nullable final List<TaskDTO> tasks) {
        int index = 1;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    public void showTask(final TaskDTO task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        @NotNull final Status status = task.getStatus();
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
    }

}
