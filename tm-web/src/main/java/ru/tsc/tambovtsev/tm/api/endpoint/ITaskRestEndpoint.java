package ru.tsc.tambovtsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

@RequestMapping("/api/tasks")
public interface ITaskRestEndpoint {

    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/findById/{id}")
    Task findById(@NotNull @PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@NotNull @PathVariable("id") String id);

    @PostMapping("/save")
    Task save(@NotNull @RequestBody Task task);

    @PostMapping("/delete")
    void delete(@NotNull @RequestBody Task task);

    @PostMapping("/deleteAll")
    void clear(@NotNull @RequestBody List<Task> task);

    @DeleteMapping("/clear")
    void clear();

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @GetMapping("/count")
    long count();

}
