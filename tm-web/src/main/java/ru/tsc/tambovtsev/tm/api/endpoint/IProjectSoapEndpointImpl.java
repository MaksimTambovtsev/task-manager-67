package ru.tsc.tambovtsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import ru.tsc.tambovtsev.tm.endpoint.ProjectSoapEndpointImpl;
import ru.tsc.tambovtsev.tm.soap.project.*;

public interface IProjectSoapEndpointImpl {

    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectCreateResponse create(
            @NotNull
            @RequestPayload ProjectCreateRequest request
    );

    @PayloadRoot(localPart = "projectFindAllRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectFindAllResponse findAll(
            @NotNull
            @RequestPayload ProjectFindAllRequest request
    );

    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectFindByIdResponse findById(
            @NotNull
            @RequestPayload ProjectFindByIdRequest request
    );

    @PayloadRoot(localPart = "projectSaveRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectSaveResponse save(
            @NotNull
            @RequestPayload ProjectSaveRequest request
    );

    @PayloadRoot(localPart = "projectDeleteRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectDeleteResponse delete(
            @NotNull
            @RequestPayload ProjectDeleteRequest request
    );

    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectDeleteAllResponse deleteAll(
            @NotNull
            @RequestPayload ProjectDeleteAllRequest request
    );

    @PayloadRoot(localPart = "projectClearRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectClearResponse clear(
            @NotNull
            @RequestPayload ProjectClearRequest request
    );

    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectDeleteByIdResponse deleteById(
            @NotNull
            @RequestPayload ProjectDeleteByIdRequest request
    );

    @PayloadRoot(localPart = "projectCountRequest", namespace = ProjectSoapEndpointImpl.NAMESPACE)
    ProjectCountResponse count(
            @NotNull
            @RequestPayload ProjectCountRequest request
    );

}
