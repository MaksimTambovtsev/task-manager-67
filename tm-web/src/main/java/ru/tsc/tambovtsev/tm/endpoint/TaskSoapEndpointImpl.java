package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskSoapEndpointImpl;
import ru.tsc.tambovtsev.tm.service.TaskService;
import ru.tsc.tambovtsev.tm.soap.task.*;

import java.util.stream.Collectors;

@Endpoint
public class TaskSoapEndpointImpl implements ITaskSoapEndpointImpl {

    @NotNull
    public static final String LOCATION_PATH = "/ws";

    @NotNull
    public static final String PORT_NAME = "/TaskSoapEndpointPort";

    @NotNull
    public static final String NAMESPACE = "http://tsc.ru/tambovtsev/tm/soap/task";

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateRequest", namespace = NAMESPACE)
    public TaskCreateResponse create(
            @NotNull
            @RequestPayload final TaskCreateRequest request
    ) {
        @NotNull final TaskCreateResponse response = new TaskCreateResponse();
        response.setTask(taskService.create(request.getName()));
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(
            @NotNull
            @RequestPayload final TaskFindAllRequest request
    ) {
        @NotNull final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTask(taskService
                .findAll()
                .stream()
                .collect(Collectors.toList())
        );
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(
            @NotNull
            @RequestPayload final TaskFindByIdRequest request
    ) {
        @NotNull final TaskFindByIdResponse response = new TaskFindByIdResponse();
        response.setTask(taskService.findById(request.getId()));
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(
            @NotNull
            @RequestPayload final TaskSaveRequest request
    ) {
        taskService.save(request.getTask());
        return new TaskSaveResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(
            @NotNull
            @RequestPayload final TaskDeleteRequest request
    ) {
        taskService.remove(request.getTask());
        return new TaskDeleteResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse clear(
            @NotNull
            @RequestPayload final TaskDeleteAllRequest request
    ) {
        taskService.remove(request.getTask());
        return new TaskDeleteAllResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(
            @NotNull
            @RequestPayload final TaskClearRequest request
    ) {
        taskService.clear();
        return new TaskClearResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(
            @NotNull
            @RequestPayload final TaskDeleteByIdRequest request
    ) {
        taskService.removeById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(
            @NotNull
            @RequestPayload final TaskCountRequest request
    ) {
        @NotNull final TaskCountResponse response = new TaskCountResponse();
        response.setCount(taskService.count());
        return response;
    }
    
}
